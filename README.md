***PIXI and Webpack Boilerplate***


This is just another empty boilerplate just to help people getting started using the wonderful PIXIJS library.

Clone it, and run:

**npm install**

After everything is installed, if you wish to start developing, run

**npm run dev**

It will initialize the webpack dev server on localhost:8080

If everything went correctly, you will see a green canvas on the aforementioned address.

To build a distribution copy of what you developed, simple run:

**npm run build**