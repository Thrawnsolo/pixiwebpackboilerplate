const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const basePath = path.resolve(__dirname, '../');
const APP_DIR = path.resolve(basePath, './src');

const pixi = path.join(basePath, '/node_modules/pixi.js/');

module.exports = {
    entry: {
        app: [
            'babel-polyfill',
            'pixi.js',
            `${APP_DIR}/main.js`
        ]
    },
    output: {
        path: path.resolve(basePath, 'dist/'),
        filename: '[name].bundle.js'
    },
    resolve: {
        alias: { pixi }
    },
    module: {
        rules: [
            { test: /\.js$/, use: ['babel-loader'], include: APP_DIR },
            { test: /\.css$/, use: ['style-loader', 'css-loader'] },
            {
                test: /\.(jpe?g|png|svg|gif)$/i,
                loaders: ['file-loader?name=[name].[ext]']
            },
            { test: /\.(woff|woff2|eot|ttf|otf)$/, use: ['file-loader'] },
            { test: /\.(ogg|mp3)$/, use: ['file-loader'] }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([
            {
                from: 'src/assets',
                to: 'assets'
            }
        ]),
        new HtmlWebpackPlugin({
            title: 'Production',
            template: './src/index.html'
        }),
        new CleanWebpackPlugin(['dist'], {
            root: `${basePath}`
        })
    ]
};
